# Privacy Modules Google PlayStore specific implementations

This Android Module (aar) implements PrivacyModules functionalities respecting all the constraints to be available on the Google PlayStore.



# Functionalities

## v0.0.2

* Change runtime permission automatically using Accessibility.
* Set mock location API using accessibility.

# Structure (UML class diagrams)

* v0.0.2.1 (Permissions)

```mermaid
classDiagram
    AbstractPermissionHelper <|-- GooglePermissionHelper
    GooglePermissionHelper ..|> IAccessibilityEnable
    AbstractPrivacyLeakageMeter <|-- GooglePrivacyLeakageMeter
    AbstractPermissionManager <|-- GooglePermissionManager

    class IAccessibilityEnable{
    <<interface>>
    +isAccessibilityEnabled()*
    }

    class GooglePermissionHelper{
    +isAccessibilityEnabled()
    }

```
