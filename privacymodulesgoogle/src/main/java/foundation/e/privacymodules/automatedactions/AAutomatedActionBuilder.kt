package foundation.e.privacymodules.automatedactions

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager

abstract class AAutomatedActionBuilder(protected val context: Context) {
    protected val pm get() = context.packageManager

    protected fun getActivityInfo(intent: Intent): ActivityInfo? {
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        val info = context.packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return info?.activityInfo
    }

    protected fun getLabel(packageName: String?, resourceName: String?): CharSequence? {
        if (packageName == null || resourceName == null) {
            return null
        }
        val res = context.packageManager.getResourcesForApplication(packageName)
        val resID = res.getIdentifier("$packageName:string/$resourceName", null, null)

        return context.packageManager.getText(packageName, resID, null)
    }

    protected fun buildBackStep(explanations: String, forwardStep: AutomatedActionStep): AutomatedActionStep {
        return AutomatedActionStep(
            explanations = explanations,
            packageName = forwardStep.packageName,
            checks = forwardStep.checks,
            actions = listOf(Action(selector = null, actionId = AccessibilityService.GLOBAL_ACTION_BACK))
        )
    }

    protected abstract fun buildStartIntent(): Intent
    protected abstract fun buildFirstStep(): AutomatedActionStep?

    public abstract fun build(): AutomatedAction?
}