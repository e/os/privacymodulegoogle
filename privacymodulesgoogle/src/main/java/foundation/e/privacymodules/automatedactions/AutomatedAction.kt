package foundation.e.privacymodules.automatedactions

import android.content.Intent
import foundation.e.privacymodules.commons.ManualActionCode


data class AutomatedAction(
    val intent: Intent,
    val actionCode: ManualActionCode,
    val steps: MutableList<AutomatedActionStep>,
    val startedAt: Long,
    val maxDuration: Int = 2000
) {
    fun isFresh() = (startedAt + maxDuration) > System.currentTimeMillis()
}

data class AutomatedActionStep(
    val explanations: String,
    val packageName: String?,
    val checks: List<Selector>,
    val actions: List<Action>
)

data class Selector(val text: String? = null, val selectParent: Boolean = false)

data class Action(
    val selector: Selector?,
    val actionId: Int
)