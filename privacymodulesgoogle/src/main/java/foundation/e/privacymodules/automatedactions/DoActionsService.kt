package foundation.e.privacymodules.automatedactions

import android.accessibilityservice.AccessibilityService
import android.graphics.PixelFormat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityNodeInfo.ACTION_SCROLL_FORWARD
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import foundation.e.privacymodules.BuildConfig
import foundation.e.privacymodules.commons.Logger
import foundation.e.privacymodules.google.R
import java.util.*

class DoActionsService: AccessibilityService() {
    companion object {
        /**
         * Set an action to perform through Accessibility features.
         */
        fun setActionToPerform(action: AutomatedAction) {
            shouldStop = false
            actionToPerform = action
        }

        /**
         * @property actionToPerform the current action being performed. Null if none actually in
         * progress.
         */
        private var actionToPerform: AutomatedAction? = null

        /**
         * @property shouldStop flag to stop the current action.
         */
        private var shouldStop = true

        private const val ACTION_TAG = "A11yActioning"
        private const val DISPLAY_TAG = "A11yDisplay"
    }

    /**
     * @property viewDisplayed the accessibility overlay view displayed when an action is performing.
     */
    private var viewDisplayed: View? = null

    /**
     * Perform check before continuing actions.
     * @return true if the actions should stop.
     */
    private fun shouldStop(): Boolean {
        if (!shouldStop) {
            shouldStop = actionToPerform?.isFresh() != true
        }

        return shouldStop
    }

    /**
     * Mark the current step as done and remove it from the actions list.
     * @param step the step to remove from the current action.
     */
    private fun markStepDone(step: AutomatedActionStep) {
        Logger.d(ACTION_TAG, "remove step ${actionToPerform?.steps?.indexOf(step)}")
        actionToPerform?.steps?.remove(step)

        if (actionToPerform?.steps?.isNullOrEmpty() == true) {
            cleanAction()
        }
    }

    /**
     * Reset variables when action is done (finished or aborted).
     */
    private fun cleanAction() {
        actionToPerform = null
        shouldStop = true
        hideView()
    }

    /**
     * Stop the actions when the user click on the stop button on accessibility overlay view.
     */
    private fun onClickStop() {
        shouldStop = true
    }

    /**
     * Show the accessibility overlay view with the specified explanation.
     * @param explanations a description of what the action is actually performing.
     */
    private fun displayView(explanations: String) {
        if (viewDisplayed == null) {
            val windowManager: WindowManager = getSystemService(WINDOW_SERVICE) as WindowManager

            val layout = FrameLayout(this)
            val params: WindowManager.LayoutParams = WindowManager.LayoutParams()
            params.height = WindowManager.LayoutParams.WRAP_CONTENT
            params.width = WindowManager.LayoutParams.MATCH_PARENT
            params.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY
            params.flags = params.flags or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
            params.format = PixelFormat.TRANSLUCENT
            params.gravity = Gravity.BOTTOM

            LayoutInflater.from(this).inflate(R.layout.action_bar, layout)

            windowManager.addView(layout, params)
            layout.findViewById<Button>(R.id.stop).setOnClickListener { onClickStop() }

            viewDisplayed = layout
        }

        viewDisplayed?.findViewById<TextView>(R.id.explanations)?.text = explanations
    }

    /**
     * Hide the accessibility overlay view.
     */
    private fun hideView() {
        if (viewDisplayed != null) {
            val windowManager: WindowManager = getSystemService(WINDOW_SERVICE) as WindowManager
            windowManager.removeView(viewDisplayed)
            viewDisplayed = null
        }
    }

    /**
     * Return the first [AccessibilityNodeInfo] with scollable flag using a in depth search.
     * @param nodeInfo the start node.
     * @return the scrollable node, or null if none found.
     */
    private fun findScrollableView(nodeInfo: AccessibilityNodeInfo): AccessibilityNodeInfo? {
        return searchInDepth(nodeInfo) { it.isScrollable }
    }

    private fun findNodeByTruncatedText(nodeInfo: AccessibilityNodeInfo, text: String?): AccessibilityNodeInfo? {
        var res = nodeInfo.findAccessibilityNodeInfosByText(text)
        if (res.isNotEmpty()) {
            return res.firstOrNull()
        }

        if (text == null) {
            return null
        }

        val matches = LinkedList<AccessibilityNodeInfo>()
        searchInDepth(nodeInfo) {
            // Truncated text ends by a '.'
            if (it.text != null && it.text.endsWith(".") &&
                text.startsWith(it.text.removeSuffix("."))) {
                matches.add(it)
            }
            false
        }

        return matches.sortedBy { it.text.length }.lastOrNull()
    }

    /**
     * Return the first [AccessibilityNodeInfo] matching the [test] using a in depth search.
     * @param nodeInfo the start node.
     * @param test a lambda returning a boolean
     * @return the scrollable node, or null if none found.
     */
    private fun searchInDepth(nodeInfo: AccessibilityNodeInfo, test: (AccessibilityNodeInfo) -> Boolean): AccessibilityNodeInfo? {
        if (test(nodeInfo)) {
            return nodeInfo
        }

        for (i in 0 until nodeInfo.childCount) {
            val res = searchInDepth(nodeInfo.getChild(i), test)
            if (res != null) {
                return res
            }
        }
        return null
    }

    /**
     * Perform scroll actions until the action.selector is found. Check for shouldStop to avoid
     * infinite blocking.
     * @param action the scroll action to perform.
     * @return the expected node after scroll actions.
     */
    private fun scrollTo(action: Action): AccessibilityNodeInfo? {
        val scrollable = findScrollableView(rootInActiveWindow)
        if (scrollable != null) {
            while (!shouldStop() && rootInActiveWindow.findAccessibilityNodeInfosByText(action.selector?.text)
                    .isNullOrEmpty()
            ) {
                scrollable.performAction(action.actionId)
            }
        }

        return rootInActiveWindow.findAccessibilityNodeInfosByText(action.selector?.text).firstOrNull()
    }

    /**
     * Called by the system each time an accessibility event we have registrere for is fired.
     */
    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        if (shouldStop()) {
            cleanAction()
            return
        }

        val step = actionToPerform?.steps?.firstOrNull()
        if (step == null) {
            Logger.d(ACTION_TAG, "NO more actions")
            cleanAction()
            return
        }

        Logger.d(ACTION_TAG, "starting step: ${step.explanations}")
        displayView(step.explanations)

        // Check we are on the expected screen.
        if (step.packageName == null ||
            rootInActiveWindow?.packageName == step.packageName && step.checks.all { selector ->
                Logger.d(ACTION_TAG, "Check selector: ${selector.text}")
                findNodeByTruncatedText(rootInActiveWindow, selector.text) != null
            }
            ) {
                // We are on the expected screen, perform actions.
                step.actions.forEach { action ->
                    if (shouldStop()) {
                        return@forEach
                    }
                    Logger.d(ACTION_TAG, "Passed selector, do ${action.selector?.text}")

                    when {
                        action.actionId == ACTION_SCROLL_FORWARD -> scrollTo(action)
                        action.selector?.text != null -> {
                            var node = findNodeByTruncatedText(rootInActiveWindow, action.selector.text)
                            if (action.selector.selectParent) {
                                node = node?.parent
                            }
                            node?.performAction(action.actionId)
                        }
                        else -> performGlobalAction(action.actionId)
                    }
                }
            markStepDone(step)
        }

        // To help debug,
        if (BuildConfig.DEBUG) {
            Logger.d(DISPLAY_TAG, "packageName: ${rootInActiveWindow?.packageName}")
            logViewHierarchy(rootInActiveWindow, 0)
        }
    }

    /**
     * Helper to display view hierarchy
     */
    private fun logViewHierarchy(nodeInfo: AccessibilityNodeInfo?, depth: Int) {
        if (nodeInfo == null) return
        var spacerString = ""
        for (i in 0 until depth) {
            spacerString += '-'
        }
        nodeInfo.refresh()

        //Log the info you care about here... I choose classname and view resource name, because
        // they are simple, but interesting.
        Logger.d(
            DISPLAY_TAG,
            "$spacerString ${nodeInfo.className} ${nodeInfo.viewIdResourceName} ${nodeInfo.text} ${nodeInfo.contentDescription} ${nodeInfo.isImportantForAccessibility}"
        )
        for (i in 0 until nodeInfo.childCount) {
            logViewHierarchy(nodeInfo.getChild(i), depth + 1)
        }
    }

    /**
     *  This method is called when the system wants to interrupt the feedback your service is
     *  providing, usually in response to a user action such as moving focus to a different control.
     *  This method may be called many times over the lifecycle of your service.
     */
    override fun onInterrupt() {}
}
