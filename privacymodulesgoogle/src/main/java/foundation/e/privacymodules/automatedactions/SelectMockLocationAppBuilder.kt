package foundation.e.privacymodules.automatedactions

import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.view.accessibility.AccessibilityNodeInfo
import foundation.e.privacymodules.commons.ManualActionCode
import foundation.e.privacymodules.google.R
import foundation.e.privacymodules.permissions.data.ApplicationDescription

class SelectMockLocationAppBuilder(context: Context, private val appDesc: ApplicationDescription)
    :AAutomatedActionBuilder(context) {

    override fun buildStartIntent(): Intent {
        return Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS)
    }

    override fun buildFirstStep(): AutomatedActionStep? {
        val devIntent = buildStartIntent()

        val devInfos = getActivityInfo(devIntent)
        val buttonLabel = getLabel(devInfos?.packageName, "mock_location_app")

        if (devInfos == null || buttonLabel == null) {
            return null
        }

        return AutomatedActionStep(
            explanations = context.getString(R.string.accessibility_step_select_mock_location_1, buttonLabel),
            packageName = devInfos.packageName,
            checks = listOf(Selector(devInfos.loadLabel(pm).toString())),
            actions = listOf(
                Action(selector = Selector(buttonLabel.toString()),
                    actionId = AccessibilityNodeInfo.ACTION_SCROLL_FORWARD
                ),
                Action(
                    selector = Selector(text = buttonLabel.toString(), selectParent = true),
                    actionId = AccessibilityNodeInfo.ACTION_CLICK
                )
            )
        )
    }

    private fun buildSecondStep(): AutomatedActionStep? {
        return AutomatedActionStep(
            explanations = context.getString(R.string.accessibility_step_select_mock_location_2, appDesc.label),
            packageName = null,
            checks = emptyList(),
            actions = listOf(Action(selector = Selector(text = appDesc.label.toString(), selectParent = true),
                actionId = AccessibilityNodeInfo.ACTION_CLICK))
        )
    }



    override fun build(): AutomatedAction? {
        val firstStep = buildFirstStep()
        val secondStep = buildSecondStep()

        if (firstStep == null || secondStep == null) {
            return null
        }

        return AutomatedAction(
            actionCode = ManualActionCode.SELECT_MOCK_LOCATION_APP,
            startedAt = System.currentTimeMillis(),
            intent = buildStartIntent(),
            steps = mutableListOf(
                firstStep,
                secondStep,
                buildBackStep(context.getString(R.string.accessibility_step_select_mock_location_3),
                    firstStep)
                )
        )
    }


}