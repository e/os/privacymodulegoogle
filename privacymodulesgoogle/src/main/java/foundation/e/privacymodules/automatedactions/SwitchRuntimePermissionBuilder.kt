package foundation.e.privacymodules.automatedactions

import android.Manifest.permission_group.LOCATION
import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.accessibility.AccessibilityNodeInfo
import foundation.e.privacymodules.commons.ManualActionCode
import foundation.e.privacymodules.google.R
import foundation.e.privacymodules.permissions.data.ApplicationDescription

class SwitchRuntimePermissionBuilder(context: Context,
                                     private val appDesc: ApplicationDescription,
                                     private val permissionLabel: String,
                                     private val permissionGroup: String)
    : AAutomatedActionBuilder(context) {
    override fun buildStartIntent(): Intent {
        val settingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        settingsIntent.addCategory(Intent.CATEGORY_DEFAULT)
        settingsIntent.data = Uri.fromParts("package", appDesc.packageName, null)
        return settingsIntent
    }

    override fun buildFirstStep(): AutomatedActionStep? {
        // Building first step: click on the Autorization menu entry on app details settings view.
        val settingsInfos = getActivityInfo(buildStartIntent())

        val buttonLabel = getLabel(settingsInfos?.packageName, "permissions_label")

        // If infos where not available, abort.
        if (settingsInfos == null ||buttonLabel == null) {
            return null
        }

        return AutomatedActionStep(
            explanations = context.getString(R.string.accessibility_step_switch_runtime_permission_1, buttonLabel),
            packageName = settingsInfos.packageName, //"com.android.settings",
            checks = listOf(
                Selector(text = settingsInfos.loadLabel(pm).toString()),
                Selector(text = appDesc.label.toString())
            ),
            actions = listOf(Action(
                selector = Selector(text = buttonLabel.toString(), selectParent = true),
                actionId = AccessibilityNodeInfo.ACTION_CLICK
            ))
        )
    }

    private fun buildSecondStep(): AutomatedActionStep? {
        // Build second step: switch the permissions on manage app permissions view.
        val managePermInfos = getActivityInfo(Intent("android.intent.action.MANAGE_PERMISSIONS"))

        // if info are not valid, abort.
        if (managePermInfos == null) {
            return null
        }

        return AutomatedActionStep(
            explanations = context.getString(R.string.accessibility_step_switch_runtime_permission_2, permissionLabel),
            packageName = managePermInfos.packageName,
            checks = listOf(
                Selector(text = managePermInfos.loadLabel(pm).toString()),
                Selector(text = appDesc.label.toString())
            ),
            actions = listOf(Action(
                selector = Selector(text = permissionLabel, selectParent = true),
                actionId = AccessibilityNodeInfo.ACTION_CLICK
            ),
                Action(selector = null, actionId = AccessibilityService.GLOBAL_ACTION_BACK)
            )
        )
    }

    // Only since Android 10 (Q)
    private fun buildThirdStep(): AutomatedActionStep? {
        //cf : https://github.com/aosp-mirror/platform_packages_apps_packageinstaller/blob/c343b0516a0ff61500e1fd27a493ef3ca579855c/src/com/android/packageinstaller/permission/ui/handheld/AppPermissionFragment.java
        // Build second step: switch the permissions on manage app permissions view.
        val managePermInfos = getActivityInfo(Intent("android.intent.action.MANAGE_PERMISSIONS"))
        if (managePermInfos == null) {
            return null
        }

        val title = String.format(getLabel(managePermInfos.packageName, "app_permission_title").toString(), permissionLabel)


        val buttonLabel = getLabel(managePermInfos.packageName,
            if (permissionGroup == LOCATION) "grant_dialog_button_allow_background" else "grant_dialog_button_allow"
        )

        return AutomatedActionStep(
            explanations = context.getString(R.string.accessibility_step_switch_runtime_permission_2, permissionLabel),
            packageName = managePermInfos.packageName,
            checks = listOf(
                Selector(text = title),
                Selector(text = appDesc.label.toString())
            ),
            actions = listOf(Action(
                selector = Selector(buttonLabel.toString()),
                actionId = AccessibilityNodeInfo.ACTION_CLICK
            ),
                Action(selector = null, actionId = AccessibilityService.GLOBAL_ACTION_BACK)
            )
        )
    }


    override fun build(): AutomatedAction? {
        val firstStep = buildFirstStep()
        val secondStep = buildSecondStep()

        if (firstStep == null || secondStep == null) {
            return null
        }

        val steps = mutableListOf(
            firstStep,
            secondStep)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val thirdStep = buildThirdStep()
            if (thirdStep == null) {
                return null
            }

            steps.add(thirdStep)
            steps.add(buildBackStep(context.getString(R.string.accessibility_step_switch_runtime_permission_3),
                secondStep))
        }

        steps.add(buildBackStep(context.getString(R.string.accessibility_step_switch_runtime_permission_3),
            firstStep))


        return AutomatedAction(
            actionCode = ManualActionCode.SWITCH_RUNTIME_PERMISSION,
            startedAt = System.currentTimeMillis(),
            intent = buildStartIntent(),
            steps = steps)
    }
}

/*
<string name="grant_dialog_button_allow" msgid="2137542756625939532">"Autoriser"</string>
<string name="grant_dialog_button_allow_always" msgid="4201473810650722162">"Toujours autoriser"</string>
<string name="grant_dialog_button_allow_foreground" msgid="3921023528122697550">"Lorsque vous utilisez l\'application"</string>
<string name="grant_dialog_button_allow_one_time" msgid="3290372652702487431">"Uniquement cette fois-ci"</string>
<string name="grant_dialog_button_allow_background" msgid="3190568549032350790">"Toujours autoriser"</string>
<string name="grant_dialog_button_allow_all_files" msgid="1581085085495813735">"Autoriser la gestion de tous les fichiers"</string>
<string name="grant_dialog_button_allow_media_only" msgid="3516456055703710144">"Autoriser l\'accès aux fichiers multimédias"</string>
        */