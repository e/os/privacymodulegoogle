package foundation.e.privacymodules.permissions

import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import foundation.e.privacymodules.automatedactions.*
import foundation.e.privacymodules.commons.ManualAction
import foundation.e.privacymodules.commons.ManualActionCode
import foundation.e.privacymodules.google.R
import foundation.e.privacymodules.permissions.data.AppOpModes
import foundation.e.privacymodules.permissions.data.ApplicationDescription

/**
* Implements [IPermissionsPrivacyModule] using only API authorized on the PlayStore.
*/
class PermissionsPrivacyModule(context: Context): APermissionsPrivacyModule(context) {
//    /**
//     * @see IPermissionsPrivacyModule.getPermissionStatus
//     */
//    override fun getPermissionStatus(uid: Int, packageName: String, permissionName: String): PermissionStatus {
//        return when(context.packageManager.checkPermission(permissionName, packageName)) {
//            PERMISSION_GRANTED -> PermissionStatus.GRANTED
//            else -> PermissionStatus.DENIED
//        }
//    }

    /**
     * @see IPermissionsPrivacyModule.toggleDangerousPermission
     * Return an ManualAction to go toggle manually the permission in the ap page of the settings.
     */
    override fun toggleDangerousPermission(
        appDesc: ApplicationDescription,
        permissionName: String,
        grant: Boolean): ManualAction? {
        val pm = context.packageManager


        val settingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        settingsIntent.addCategory(Intent.CATEGORY_DEFAULT)
        settingsIntent.data = Uri.fromParts("package", appDesc.packageName, null)

        // Get the permission group label.
        val group = getPermissionGroup(permissionName)?: ""
        val permissionLabel = if (group != null) {
            pm.getPermissionGroupInfo(group, 0).loadLabel(pm).toString()
        } else {
            permissionName
        }

        val manualAction = ManualAction(
            ManualActionCode.SWITCH_RUNTIME_PERMISSION,
            String.format(
                context.getString(R.string.manual_action_switch_runtime_permission_title_format),
                permissionLabel,
                appDesc.label
            ),
            String.format(
                context.getString(R.string.manual_action_switch_runtime_permission_instructions_format),
                permissionLabel, appDesc.label
            ),
            settingsIntent
        )

        if (isAccessibilityEnabled() != true) {
            return manualAction
        }

        val action = SwitchRuntimePermissionBuilder(context, appDesc, permissionLabel, group).build()
        if (action == null) {
            return manualAction
        }

        DoActionsService.setActionToPerform(action)
        context.startActivity(action.intent)
        return null
    }


    override fun setAppOpMode(
        appDesc: ApplicationDescription,
        permissionName: String,
        mode: AppOpModes
    ): ManualAction? {
        return if (permissionName == AppOpsManager.OPSTR_MOCK_LOCATION
            && appDesc.packageName == context.packageName
            && mode == AppOpModes.ALLOWED) {
            setMockLocationPermission(appDesc)
        } else {
            // Error, can't do anithing here.
            null
        }
    }

    private fun setMockLocationPermission(appDesc: ApplicationDescription): ManualAction? {
        // TODO activate dev mode.

        if (isDevModeEnabled() != true) {
            return ManualAction(
                context,
                ManualActionCode.ENABLE_DEVELOPER_SETTINGS,
                R.string.manual_action_enable_developer_settings_title,
                R.string.manual_action_enable_developer_settings_instructions,
                Intent(Settings.ACTION_DEVICE_INFO_SETTINGS))
        }

        // Devmode already activated, now select this has mock location app.

        val manualAction = ManualAction(
            context,
            ManualActionCode.SELECT_MOCK_LOCATION_APP,
            R.string.manual_action_select_mock_location_app_title,
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                R.string.manual_action_select_mock_location_app_instructions_under_m
            else R.string.manual_action_select_mock_location_app_instructions,
            Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS))


        if (isAccessibilityEnabled() != true) {
            return manualAction
        }

        val action = SelectMockLocationAppBuilder(context, appDesc).build()
        if (action == null) {
            return manualAction
        }

        DoActionsService.setActionToPerform(action)
        context.startActivity(action.intent)
        return null
    }

    /**
     * @see [IPermissionsPrivacyModule.isAccessibilityEnabled]
     */
    override fun isAccessibilityEnabled(): Boolean? {
        val prefString: String? = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
        )

        return prefString?.contains("${context.packageName}/foundation.e.privacymodules.automatedactions.DoActionsService"
        ) == true
    }

    private fun isDevModeEnabled(): Boolean {
        //24 - 25 (nougat): trouble builds, for some reason these builds always return true regardless of dev options enabled or not (dialog shown on first run for these users)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Settings.Secure.getInt(
                context.contentResolver,
                Settings.Global.DEVELOPMENT_SETTINGS_ENABLED,
                0
            ) != 0
        } else {
            //17 - 23: dev options enabled by default but still return default false value, so set default to true here
            Settings.Secure.getInt(
                context.contentResolver,
                Settings.Global.DEVELOPMENT_SETTINGS_ENABLED,
                1
            ) != 0
        }
    }
}
